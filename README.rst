.. -*- restructuredtext -*-

==============================================================================
*An efficient and parallel abstract interpreter in Scala* — Preparatory Work —
==============================================================================
MEMO-F403 *Preparatory work for the master thesis* (ULB)

* `Main document`_ (August 2017)
* Slides of a very brief Presentation_ (August 2017) (on Speaker Deck: https://speakerdeck.com/opimedia/an-efficient-and-parallel-abstract-interpreter-in-scala-preparatory-work-presentation )
* `Extended bibliography`_
* `Statement of the subject`_
* GitHub repository of `Scala-AM`_, the framework of Abstracting Abstract Machines that will be (partially) parallelized

.. _`Extended bibliography`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala-preparatory/raw/master/Efficient-Parallel-Abstract-Interpreter-in-Scala--Preparatory--extended-bibliography--Olivier-Pirson-2017.pdf
.. _`Main document`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala-preparatory/raw/master/Efficient-Parallel-Abstract-Interpreter-in-Scala--Preparatory--Olivier-Pirson-2017.pdf
.. _Presentation: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala-preparatory/raw/master/Efficient-Parallel-Abstract-Interpreter-in-Scala--Preparatory--Olivier-Pirson-2017--slides.pdf
.. _`Scala-AM`: https://github.com/acieroid/scala-am
.. _`Statement of the subject`: https://web.archive.org/web/20181229145848/http://soft.vub.ac.be/soft/proposal/efficient-and-parallel-abstract-interpreter-scala



You can **see the following work**, the `master thesis`_.

.. _`master thesis`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala



All documents and LaTeX sources are available on this Bitbucket repository:
https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala-preparatory

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png



|An efficient and parallel abstract interpreter in Scala - Preparatory Work|

("vectorized" piece of René Magritte with coffee stain, `Le Calcul Mental`_, 1940)

.. _`Le Calcul Mental`: http://www.artnet.com/artists/ren\%C3\%A9-magritte/le-calcul-mental-oU6yWQzE-ERgxZSTgANE-g2

.. |An efficient and parallel abstract interpreter in Scala - Preparatory Work| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/16/2777255907-3-efficient-parallel-abstract-interpret_avatar.png
