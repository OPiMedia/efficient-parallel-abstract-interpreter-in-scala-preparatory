#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
ps_offset_twoside.py offset srcname destname

(August 20, 2017)

Require psselect and psmerge programs.

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

import os
import re
import subprocess
import sys


def convert_length(length):
    assert isinstance(length, str), type(length)

    match = re.match(r'(\d*(\.\d*)?)(bp|cm|in|mm|pt)?$', length)

    number = float(match.group(1))
    unit = match.group(3)

    if unit == 'bp':    # big point: bp is the PostScript unit
        pass
    elif unit == 'cm':  # centimeter: 2.54cm = 1in
        number = number*72/2.54
    elif unit == 'in':  # inch: 1in = 72bp
        number *= 72
    elif unit == 'mm':  # millimeter: 10mm = 1cm
        number = number*72/25.4
    elif unit == 'pt':  # point (TeX unit): 72.27pt = 72bp
        number = number*72/72.27
    else:
        assert unit is None, unit

    return (int(number) if number == int(number)
            else number)


def merge(oddname, evenname, destname):
    assert isinstance(oddname, str), type(oddname)
    assert isinstance(evenname, str), type(evenname)
    assert isinstance(destname, str), type(destname)

    subprocess.run(('psmerge', '-o{}'.format(destname), oddname, evenname))


def rearrange(srcname, destname, odd_count, even_count):
    assert isinstance(srcname, str), type(srcname)
    assert isinstance(destname, str), type(destname)

    assert isinstance(odd_count, int), type(odd_count)
    assert odd_count > 0, odd_count

    assert isinstance(even_count, int), type(even_count)
    assert even_count > 0, even_count

    assert (odd_count == even_count) or (odd_count == even_count + 1)

    pages = []
    for i in range(1, even_count + 1):
        pages.append(str(i))
        pages.append(str(odd_count + i))
    for i in range(even_count + 1, odd_count + 1):
        pages.append(str(i))

    subprocess.run(('psselect', '-p{}'.format(','.join(pages)),
                    srcname, destname))


def select(srcname, destname, odd=True):
    assert isinstance(srcname, str), type(srcname)
    assert isinstance(destname, str), type(destname)
    assert isinstance(odd, bool), type(odd)

    with subprocess.Popen(('psselect', ('-o' if odd
                                        else '-e'),
                           srcname, destname), stderr=subprocess.PIPE) as proc:
        output = str(proc.stderr.read())

    match = re.search(r'\s+(\d+)\s+page', output)
    if match:
        count = int(match.group(1))

        return count


def translate(page_count, offset, filename, odd=True):
    assert isinstance(page_count, int), type(page_count)
    assert isinstance(offset, int) or isinstance(offset, float), type(offset)
    assert isinstance(filename, str), type(filename)
    assert isinstance(odd, bool), type(odd)

    lines = []
    count = 0
    with open(filename) as filein:
        for line in filein:
            lines.append(line)
            if re.match('%%Page:\s+(\d+)\s+(\d+)\s+$', line):
                count += 1
                lines.append('{} 0 translate% ps_offset_twoside\n'
                             .format(offset if odd
                                     else -offset))

    with open(filename, 'w') as fileout:
        fileout.writelines(lines)

    if count != page_count:
        print("# {}: {} replaced instead {}!".format(('odd' if odd
                                                      else 'even'),
                                                     count, page_count))


########
# Main #
########
def main():
    """
    Main function.
    """
    offset = convert_length(sys.argv[1])
    print('offset:', offset, 'bp')

    srcname = sys.argv[2]
    destname = sys.argv[3]

    oddname = '_merge_odd.ps'
    evenname = '_merge_even.ps'
    mergedname = '_merge_merged.ps'

    # Split to two files
    odd_count = select(srcname, oddname)
    print('# odd:', odd_count)

    even_count = select(srcname, evenname, odd=False)
    print('# even:', even_count)

    assert (odd_count == even_count) or (odd_count == even_count + 1)

    # Translate two files
    translate(odd_count, offset, oddname)
    translate(even_count, offset, evenname, odd=False)

    # Merge two files
    merge(oddname, evenname, mergedname)

    # Rearrange pages
    rearrange(mergedname, destname, odd_count, even_count)

    # Clean
    os.remove(oddname)
    os.remove(evenname)
    os.remove(mergedname)

if __name__ == '__main__':
    main()
