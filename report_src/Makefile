DOCUMENTS = ../Efficient-Parallel-Abstract-Interpreter-in-Scala--Preparatory--Olivier-Pirson-2017.pdf
#	../Efficient-Parallel-Abstract-Interpreter-in-Scala--Preparatory--Olivier-Pirson-2017--printable-twoside.pdf

.SUFFIXES:

BIBTEX      = biber
BIBTEXFLAGS =

DVIPS      = dvips
DVIPSFLAGS =

MAKEINDEX      = makeindex
MAKEINDEXFLAGS =

PSPDF      = ps2pdf
PSPDFFLAGS = -sPAPERSIZE=a4

TEX      = latex
TEXFLAGS =


RM    = rm -f
SHELL = sh



###
# #
###
.PHONY:	all

all:	$(DOCUMENTS)



#########
# Rules #
#########
.PRECIOUS: %.aux %.bcf %.dvi %.idx %.ind %--printable-twoside.ps %.ps

%.dvi:	%.tex
	$(TEX) $(TEXFLAGS) $<
ifeq ($(QUICK),)
	$(BIBTEX) $(BIBTEXFLAGS) $(basename $<)
	$(TEX) $(TEXFLAGS) $<
endif

%.idx:	%.tex
	$(TEX) $(TEXFLAGS) $<

%.ind:	%.idx
	$(MAKEINDEX) $(MAKEINDEXFLAGS) $<

../%.pdf:	%.ps
	$(PSPDF) $(PSPDFFLAGS) $< $@

%--printable-twoside.ps:	%.ps
	./py/ps_offset_twoside.py 1cm $< $@

%.ps:	%.dvi
	$(DVIPS) $(DVIPSFLAGS) -o $@ $<



################
# Dependancies #
################
Efficient-Parallel-Abstract-Interpreter-in-Scala--Preparatory--Olivier-Pirson-2017.dvi:	\
	Efficient-Parallel-Abstract-Interpreter-in-Scala--Preparatory--Olivier-Pirson-2017.ind \
	sty/opireport.sty bib/preparatory.bib \
	tex/abstract.tex tex/resume.tex tex/acknowledgements.tex \
	tex/overview.tex tex/foundation.tex tex/cesk.tex tex/parallelization.tex tex/future.tex \
	tex/abbreviations.tex tex/symbols.tex



#########
# Clean #
#########
.PHONY:	clean cleanDvi cleanPdf cleanPs distclean overclean

clean:
	$(RM) *.aux tex/*.aux
	$(RM) *.bbl *.bcf *.blg *.brf *.idx *.ilg *.ind *.loe *.lof *.log .log *.out *.toc *.tdo
	$(RM) Efficient-Parallel-Abstract-Interpreter-in-Scala--Preparatory--Olivier-Pirson-2017.run.xml
	$(RM) t.l

cleanDvi:
	$(RM) *.dvi

cleanPdf:
	$(RM) $(DOCUMENTS)

cleanPs:
	$(RM) *.ps

distclean:	clean cleanDvi cleanPs

overclean:	distclean cleanPdf
