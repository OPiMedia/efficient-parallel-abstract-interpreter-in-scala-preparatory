% -*- coding: utf-8 -*-
\chapter{Overview}
\label{chap:overview}%
\pagenumbering{arabic}%
Nowadays computers and programs are everywhere.
For some anodyne tasks, but as well as for a lot of very critical ones.
It is crucial that they run perfectly correctly.

In the classical example of the aviation domain,
the least mistake can have catastrophic consequences.
Other common examples are medical assistance,
management of nuclear power stations,
road traffic control,
banking flows,
stock exchange management,
etc.
We also want that our washing machines run correctly,
and our smartphones,
television sets,
etc.
Programs are really everywhere.
And \itblockquote{One Ring to rule them all}
\footnote{J. R. R. \textsc{Tolkien}, \textit{The Lord of the Rings}, 1954.},
the Internet is also a gigantic masterpiece
composed of a cloud of programs.



\section{From execution on some instances to global analysis of behaviour}
\epigraph{\itblockquote{The first moral of the story is that program testing can be used very effectively to show the presence of bugs but never to show their absence.}}
         {--- Edsger W. \textsc{Dijkstra},
           \href{https://www.cs.utexas.edu/users/EWD/transcriptions/EWD03xx/EWD303.html}
                {\textit{On the reliability of programs.} (EWD303)}}
\noindent
To be sure that a program runs correctly on \textit{each} instance
\footnote{
  In accordance with the terminology of the computational complexity theory
  we call one data of a problem
  an \index{instance}\emphDef{instance} of this problem.
  And we use the same term for the input of a program.}
we have to be sure that it runs correctly on \textit{all} instances.
It is obvious to say, but it is a big challenge.

One way to avoid mistakes in a program
is to execute it on some instances,
well chosen to explore different paths on this program.
There exists several techniques, named \index{testing}\emphDef{testing},
with different degrees of performance and precision
(unit testing, integration testing\dots).
However with such techniques, one cannot know if any problematic case was missed.
And except on particular programs it is impossible to cover all instances,
because there is a combinatorial explosion of the number of possibilities.
This kind of tests is really important, but we need something better.

The thesis of Olivier \textsc{Bouissou}
\cite{bouissou_analyse_2008}
summarizes the cycle of program development
with different possible types of tests associated on each step.
It also recalls some famous software failures,
like the explosion of Ariane 5 at its first launch.
During the investigation after this accident
another type of verification was made
(in addition to a manual code analysis).
Instead of executing the program on some instances,
it was submitted to a static analysis
to study its global behaviour.
This study found the reason of the accident.
The bug was the conversion of a floating number
too big to become an integer on the architecture used.
Too late in this case.
Nevertheless this example shows the utility of static analysis,
if it is done at the right time,
before the programs are used.

\bigskip
Beyond the necessity to verify the correctness of programs,
it is interesting to prove properties of their behaviour.
This is useful to make automatic transformations and for optimizations.
This may be necessary to ensure
that our correct programs are executed quickly enough.

\bigskip
\index{analysis!dynamic analysis}%
\index{analysis!static analysis}%
A \emphDef{dynamic analysis} of a program is made at runtime,
consequently on particular instances.
On the contrary a \emphDef{static analysis} is made without executing the program.
Moreover its goal is to be exhaustive,
to \textit{prove} some properties of the program
rather than to check specific executions of the program.

\bigskip
To make these analysis we want to use computers themselves
to help us to check that they run properly.
After all the role of computers is to automate work.
\footnote{
  The French denomination of computer science
  (\foreignlanguage{french}{\textit{informatique}})
  highlights the automatic processing of information
  (\foreignlanguage{french}{\textit{traitement automatique de l'information}}).
  While the English denomination emphasizes the computability side.
  Even though basically it is the same thing.}
Anyway we are incapable ourselves of doing this work without their help.
Yet computers cannot do everything.
Because everything is not automatable.

\subsection{The wall of the undecidability}
\epigraph{\null\hfill\itblockquote{Eternity is a very long time, especially towards the end.}}
         {--- Woody \textsc{Allen}}
\noindent
\index{undecidable}%
Let's start by reminding an impossibility.
A major result of computer science informs us
that all interesting properties about the behavior of a program are \emphDef{undecidable},
which means there does not always exist a program
to always decide whether such properties are true or not.

\begin{theorem}[\textsc{Rice}'s theorem]
  Any non-trivial semantics property of a program is undecidable. \footnotemark
\end{theorem}
\footnotetext{
  The theorem proved by Henry Gordon \textsc{Rice} in 1953 \cite{rice_classes_1953}
  should be formulated in more formal manner.
  We write here the result
  to make sense in our context.}

\index{trivial property}%
A \emphDef{trivial property}
is a property such that the answer is always the same.

\index{semantics property}%
A \emphDef{semantics property} of a program is about its behavior.

\bigskip
The princeps example is the famous halting problem.
Its undecidability was proved by Alan \textsc{Turing} in 1936.
It is impossible to write a program that always says if a program
\footnote{
  Another program, or possibly the same program.
  This is the nub of matter,
  a form of self-reference being used for the demonstration.}
stops,
starting from one instance, or if runs forever.

More precisions about these fundamental concepts of calculability
are available in the book of Michael \textsc{Sipser} \cite{sipser_introduction_2012}.

\begin{figure}[H]
  \centering
  \input{tikz/program_solve.tex}\\[3ex]
  \input{tikz/program_decide.tex}
  \caption[General representation of a program\dots]
          {General representation of a program to solve one general problem for one instance,
            and specific case of a \textit{decider}
            that \textit{always} solve one
            \index{decision problem}\emphDef{decision problem}
            (i.e. a problem that accepts only the two answers \textit{yes} or \textit{no})
            \textit{for each} instance.}
\end{figure}

We are interested here in analyzers that deal with other programs in input
to study the behaviour of these programs
(and that on all possibles instances for each of these programs).

\begin{figure}[H]
  \centering
  \input{tikz/analyzer_decide.tex}
  \caption{An analyzer is a program that get a program in input.}
\end{figure}

\bigskip
\enlargethispage{\baselineskip}%
If we still want to be able
to check the correctness of our programs
and prove some properties on their behaviour
then we must give up some aspects of our claims.
We allow the analyzer to respond ``\textit{I don't know}''
\footnote{
  It is like an absence of answer.
  Another point of view is to say that the analyzer can answer
  by the tautology: ``Yes \textit{or} no''.},
and so we remain in the domain of the decidable.

\begin{figure}[H]
  \centering
  \input{tikz/static_analyzer.tex}
  \caption[For a static analyzer it is allowed to answer that it does not know\dots]
          {For a static analyzer it is allowed to answer that it does not know,\\
            to avoid infinite running.}
\end{figure}

It is always possible,
because there always exists a program that can answer ``\textit{I don't know}'' for each instance.
Obviously such a program is useless.

The first difficulty will be to find trade-offs between calculability and utility.


\subsection{Soundness guarantee}
Since it is impossible to have both,
we must choose between soundness and completeness.
\begin{enumerate}[label=(\Roman*)]
\item
  \index{soundness}\emphDef{Soundness}:
  all properties of programs proved by the analyzer are true,
  i.e. the analyzer only gives true results.
\item
  \index{completeness}\emphDef{Completeness}:
  all true properties of programs are provable by the analyzer,
  i.e. the analyzer is capable to prove all true results.
\end{enumerate}

\bigskip
We want \textit{to prove} correctness of programs
or some properties of their behavior.
We want to be sure that a positive result (\textit{yes} answer)
given by the analyzer to be true.
We choose soundness.
Consequently we accept that when it gives a negative result,
this answer means \textit{maybe not}.

\bigskip
In other terms, we choose to reject false positive errors
and we accept to obtain false negative errors.
\begin{enumerate}[label=(\Roman*)]
\item
  \index{false!false positive}\emphDef{False positive}:
  when the answer given is yes while the correct answer is no.

  \smallskip
  In a context of question about the correctness of a program,
  this means there are undetected problems.

  \smallskip
  In a context of error detection
  it is a \index{false!false alarm}\emphDef{false alarm},
  which means that the program is correct although that the analysis said that no.
\item
  \index{false!false negative}\emphDef{False negative}:
  when the answer given is no while the correct answer is yes.

  \smallskip
  In a context of question about the correctness of a program
  it is a \index{false!false alarm}\emphDef{false alarm},
  which means that the program is correct although that the analysis said that no.

  \smallskip
  In a context of error detection,
  this means there are undetected errors.
\end{enumerate}

\bigskip
In practice analyzers can answer \textit{yes}, \textit{no} or \textit{maybe not}.
In the context of the correctness of programs,
that means \textit{yes the program is correct},
\textit{no the program is incorrect and this bug has been identified}
or \textit{maybe the program is correct because no bug have been identified,
  but maybe there exist an unidentified bug}.

\bigskip
\textit{Testing} answers to the opposite question.
Instead of answering the question \textit{my program is it correct?}
testing answers to the question \textit{is there a bug in my program?}


\subsection{Concrete interpretation, the starting point}
\index{interpretation!concrete interpretation}%
A \emphDef{concrete interpretation} (its execution by a concrete interpreter)
of one program on one instance
goes successively through by states
from the initial state to the terminal state
(if it does not run forever).
We call this (possibly infinite) succession of states
the \index{trace}\emphDef{trace} of the program for this instance.

A interpreter is also called an
\index{AM}\index{abstract machine}%
\emphDef{abstract machine} (\emphDef{AM}).
\textit{Abstract} machine here is opposed to
a physical computer.

\bigskip
\index{small-step semantics}%
With the \emphDef{small-step} semantics \cite{might_tutorial:_2011}
the concrete interpretation is expressed by
\begin{itemize}
\item
  \index{function!injection function}%
  \emphDef{inject} the program $e$ into the concrete interpreter
  in an initial state $s_0$,
\item
  \index{function!transition function}%
  with a \emphDef{transition function} to move from this state
  to the next one,
\item
  and so on, until reach a final state, or run forever.
\end{itemize}

\begin{figure}[H]
  \centering
  \input{tikz/concrete_small_step.tex}
  \caption[A trace, a concrete interpretation with small-step semantics\dots]
          {A trace, a concrete interpretation with small-step semantics,
            for one instance.}
\end{figure}

This trace may be finite or infinite,
depending on the program but also on the instance given to this program.

\bigskip
We want to prove one property of the behaviour of a program
(for example its correctness),
so we interested in \textit{all} its traces.
And we want check that all these traces
do not cross prohibited states.
As already mentioned testing cannot check all cases.
Even if in practice the number of cases is finite,
it is far too large to check of all them.

\bigskip
The left part of the figure \ref{fig:concrete}
shows an illustration of a correct program:
all its traces are acceptable.

On the right, an illustration of an incorrect program:
one trace crosses prohibited state, in red.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.49\textwidth]{eps/concrete_correct}\hfill
  \includegraphics[width=0.49\textwidth]{eps/concrete_incorrect}
  \caption[Illustration of a correct and an incorrect program\dots]
          {Illustration of a correct and an incorrect program.\\
            (These images were made from images of the conference \cite{cousot_verification_208}.)}
  \label{fig:concrete}
\end{figure}

\subsection{Abstraction to simplify}
{\newcommand{\authortext}{--- René \textsc{Magritte}, \textit{Le Calcul Mental}, 1940}%
  \setlength{\beforeepigraphskip}{0pt}%
  \renewcommand{\textflush}{flushright}%
  \epigraph{\includegraphics[trim=0 0 0 9ex,width=\widthof{\authortext${\ }^2$}]{eps/Magritte__Le_calcul_mental}}
           {\authortext{}
             \footnotemark}}
\footnotetext{
  \hrefShow{http://www.artnet.com/artists/ren\%C3\%A9-magritte/le-calcul-mental-oU6yWQzE-ERgxZSTgANE-g2}}
\noindent
Instead of checking if all traces are in inside the authorized zone,
i.e. if the semantics of the program corresponds to its specification,
we will abstract the semantics.
\footnote{
  The recording of the conference of Patrick \textsc{Cousot} \cite{cousot_verification_208}
  is a pleasant way, in French, to have an introduction to the abstract interpretation.}

\bigskip
The left part of the figure \ref{fig:abstract}
shows an illustration of a correct abstract interpretation:
the abstract semantics in green has no intersection with the prohibited zone in red
and all concrete traces are in the green zone,
therefore all concrete traces are acceptable.

On the right, an illustration of an incorrect abstract interpretation:
one concrete trace escapes to the abstract semantics,
so we cannot prove the correctness of the program.
And in this case, the program is incorrect
because this concrete trace crosses prohibited state.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.49\textwidth]{eps/abstract_correct}\hfill
  \includegraphics[width=0.49\textwidth]{eps/abstract_incorrect}
  \caption[Illustration of a correct and an incorrect abstract interpretation\dots]
          {Illustration of a correct and an incorrect abstract interpretation.\\
            (These images were made from images of the conference \cite{cousot_verification_208}.)}
  \label{fig:abstract}
\end{figure}

\index{interpretation!abstract interpretation}%
To obtain a static analysis by \emphDef{abstract interpretation}
we make two simplifications.
First we reduce the concrete infinite state space
to an abstract \textit{finite} state space.
The abstract interpreter will work with abstract values,
which represent an over-approximation of the concrete values.
\index{_abstraction@$\,\abst{\ }$}%
In general we will use the hat symbol\ \ \emphDef{$\abst{\ }$}\ \ to express
that an element is an abstraction
(or a function on abstractions).

Second we mimic the concrete small-step semantics on this abstract state space.
But instead of going from one state to another state,
the abstract transition function goes from one state to all (directly) reachable states.

One technique to build an abstract interpreter from a language semantics
is called
\index{AM!AAM}\index{abstract machine!abstracting abstract Machine}%
\emphDef{abstracting abstract machine} (\emphDef{AAM}).
This is the technique that we will use.

\bigskip
\index{state graph}%
A program trace is now approximated by a finite \emphDef{state graph},
which may contain cycles.
This state graph being finite,
it ensures that the abstract interpretation is decidable
and can be computed in a finite amount of time.

\begin{figure}[H]
  \centering
  \input{tikz/abstract_small_step.tex}
  \caption[State graph, abstract interpretation with small-step semantics\dots]
          {State graph, abstract interpretation with small-step semantics,
            for \textit{all} instances.}
\end{figure}

It should be noted that the abstract result
makes two over-approximations of the concrete result.
Abstract values over-approximate concrete values.
And the state graph forgets some order of states present in the trace.
Besides it summarizes the global behavior of the program, on all its instances.

\begin{figure}[H]
  \centering
  \input{tikz/concrete_abstract_small_step.tex}
  \caption[\itblockquote{The abstract simulates the concrete}, for \textit{all} instances.]
          {\itblockquote{The \textcolor{orange}{abstract} \textcolor{DarkGreen}{simulates} the \textcolor{red}{concrete}}
            \cite{might_tutorial:_2011},
            for \textit{all} instances.}
\end{figure}

\paragraph{Sign example}
\label{subsec:sign}%
A classical example \cite{might_what_2012}.
to illustrate the principle of abstraction,
we will represent integer numbers by their sign.
We transform concrete values in abstract values
by a \index{function!abstraction function}\emphDef{abstraction function}
generally noted $\abstraction$:
\footnote{
  We work theoretically on the infinite set of values
  although in practice programming language implementations
  can only handle a finite subset of these values.}\\[1ex]
$\integers\quad
\stackrel{\abstraction}{\longrightarrow}\quad
\abst{\integers}
= \powerset(\{-, 0, +\})
= \{\emptyset, \{-\}, \{0\}, \{+\}, \{-, 0\}, \{-, +\}, \{0, +\},\{-, 0, +\}\}$

\bigskip
We also define an abstract addition $\abst{+}$ on the abstract integers
which verifies in particular these equalities:
\footnote{
  Do not confuse the meaning of symbols.
  A same symbols may be have several meanings.
  Here and in the rest of this example,
  the meaning of the symbol $-$
  may be is the sign of a concrete integer like in $-42$
  or the meaning of a negative abstract value like in $\{-, 0\}$.
  It could be also the sign of the concrete subtraction operator like in $42 - 36$
  and the sign of the abstract subtraction operator like in $\{0\}\,\abst{-}\,\{+\}$.}\\
$\{-\}\,\abst{+}\,\{-\} = \{-\}$\\[0.5ex]
$\{-\}\,\abst{+}\,\{+\} = \{-, 0, +\}$\\[0.5ex]
$\{-, 0\}\,\abst{+}\,\{-\} = \{-\}$\\
\dots

\bigskip
With this very simple abstraction
we can study behaviour of some use of the addition $+$ on integers.
\footnote{
  Such a simple abstraction may nevertheless be useful.
  A very common verification is that check if an index on an array
  is really in this array.
  If the sign of the index is not negative ($-$)
  then we have the guarantee that the program do not try to access to a negative index.}

We can deduce that the sign of $-36 + -6$ is $-$,
by $\abstraction(-36)\,\abst{+}\,\abstraction(-6) = \{-\}$.
This is precisely the correct sign of $-42$.

We see that this abstraction may be not precise enough
to answer to all questions,
we cannot deduce the sign of $-42 + 42$
because $\abstraction(-42)\,\abst{+}\,\abstraction(42) = \{-, 0, +\}$.

\paragraph{Precision}
The \index{precision}\emphDef{precision}
of an abstraction characterize its capacity to analyze properties.
More the abstraction is close to its original concrete model
and more the abstraction can be ideally useful to study what we want.

\paragraph{Another classical abstractions}
We can use an interval to represent a number.
For example 42 may be approximated by $[0,100]$.

With intervals it is possible to have a perfect precision, like $[42,42]$.
Or less and less precision, like $[0,100]$, $[0, +\infty]$, $[-\infty, +\infty]$.

Note that $[0, +\infty]$ represents the all naturals set $\naturals$
and $[-\infty, +\infty]$ the all integers set $\integers$.
In the same way $[1,\infty]$ corresponds to the sign $+$.

\bigskip
More generally yet, we can approximate a value by a set of this type of values.
And more general yet, approximation of a value by its type (e.g. Integer).

\bigskip
The difficulties to have both an approximation
simplifying enough to be calculable in practice (to have a good complexity)
and
good enough to be ideally capable to check some properties (to have a good precision),
leads to a large variety of type of abstractions.
Polygons and polyhedrons for example allow to combine inequalities.
In a similar manner it is also possible to combine congruence equalities.
\cite{cousot_interpretation_2000}

This second difficulty, the most important in practice,
is to find trade-offs between complexity and precision.


\subsection{Fixed point search}
\index{fixed point}%
In general, a \emphDef{fixed point} of a function $f$ is a $x$ such that $f(x) = x$.

Programs generally contain loops or recursion,
thus their state graphs contains cycles.
To deal with this problem we will have to find fixed points
This task may be complicated and may require new approximations.

We will see in the chapter \ref{chap:foundation} some mathematical conditions and structures
that can to guarantee existence of these fixing points.


\subsection{The cost of the complexity}
After all these approximations,
we obtain a state graph
which allows us to answer some questions about the behaviour of our program.
Our approximated problems become decidable,
and ideally solvable.

In practice our computers have limited memory,
and we have limited time.
In general these limitations depend on the goal
(several weeks/months are acceptable for the verification of a rocket,
but at most few seconds/minutes for a static analysis in an integrated development environment.)

The complexity to prove a behaviour of a program is high.
Even on small programs the complexity explodes quickly.
For reals programs, i.e. usually large programs,
the complexity is often gigantic.
However it is precisely this kind of programs that we would like to be able to analyze.

A natural idea is to believe that a weak precision
can be calculated quickly.
It would seem that it is not so simple.

For specific problems it is eventually possible to make a good choice of abstraction
and/or optimize the implementation of the analyzer.
However a real solution would be to have a general usable solution.
If we want to improve the general techniques of abstract interpretation
as presented before, we have to formalized them precisely.
It will also be done in the chapter \ref{chap:foundation}.



\section{The \textit{Scheme} programming language, as the target}
Functional programming languages are generally not mainstream.
Perhaps because of a restrictive ``mathematical'' aspect that repel them.
And also a very refined syntax composed by a lot of nested parentheses.

Let \textsc{Abelson}, \textsc{Sussman} and \textsc{Sussman},
the authors of the classic \textit{wizard book},
give an answer to that:
``\textit{If Lisp
  \footnote{
    \textsc{Abelson}, \textsc{Sussman} and \textsc{Sussman}
    talk about \textit{Lisp},
    nevertheless in fact all the book uses \textit{Scheme},
    a direct derived of the \textit{Lisp} family language.}
  is not a mainstream language,
  why are we using it as the framework for our discussion of programming?
  Because the language possesses unique features
  that make it an excellent medium for studying important programming constructs
  and data structures and for relating them to the linguistic features that support them.
  The most significant of these features is the fact that Lisp descriptions of processes,
  called procedures, can themselves be represented and manipulated as Lisp data.
  [\dots]
  As we shall discover,
  Lisp's flexibility in handling procedures as data
  makes it one of the most convenient languages in existence for exploring these techniques.
  The ability to represent procedures as data also makes Lisp an excellent language
  for writing programs that must manipulate other programs as data,
  such as the interpreters and compilers that support computer languages.
  Above and beyond these considerations, programming in Lisp is great fun.}''
\cite{abelson_structure_1996}

It is relatively easy to implement a (concrete) \textit{Lisp} interpreter
in \textit{Lisp} itself.

Linked lists are a fundamental data structure in \textit{Lisp}.
And the program itself is a such list.
This equivalence between source code and data
is called \index{homoiconicity}\emphDef{homoiconicity}.


\pagebreak
\subsection{\textit{Scheme} presentation}
\index{Scheme}\emphDef{Scheme}
is a functional programming language
directly descending of \textit{Lisp}
and retaining its minimal syntax
(with some fundamental design features modifications
like lexical scoping).
``\textit{It was designed to have an exceptionally clear
  and simple semantics and few different ways to form expressions.
  A wide variety of programming paradigms, including imperative, functional,
  and message passing styles, find convenient expression in Scheme.}''
\cite{kelsey_revised5_1998}

\paragraph{Standards}
There are a lot of implementations of \textit{Scheme},
some with extensions (like \textit{Racket} \cite{community_racket_????}).
There are also several successive standards,
the official IEEE Standard of 1990 \cite{community_ieee_1990},
\index{RnRS@R$n$RS}%
and since, \textit{Revised$^n$ Report on the Algorithmic Language Scheme} (R$n$RS).

\paragraph{Quick enumeration of its features}
(several introduced for the first time by this language \cite{kelsey_revised5_1998}):
\begin{itemize}[noitemsep]
\item homoiconicity,
\item
  capability to manipulate mutable data
  (contrary to a purely functional programming language like
  \href{https://www.haskell.org/}{\textit{Haskell}}),
\item
  \index{typing!strong typing}\emphDef{strong typing}:
  there is no implicit conversion,
\item
  \index{typing!latent typing}\emphDef{latent typing}
  (or \index{typing!dynamic typing}\emphDef{dynamic typing}):
  types are associated with values instead variables,
\item
  \index{scope!lexical scope}\emphDef{lexical scope}
  (or \index{scope!static scope}\emphDef{static scope}):
  bindings of variables in a function
  are independent of the caller context,
\item
  \index{closure}\emphDef{closure}
  (or \index{closure!lexical closure}\emphDef{lexical closure},
  \index{closure!function closure}\emphDef{function closure}):
  function associated with its environment,
\item
  \index{first-class!first-class function}\emphDef{first-class function}:
  functions are elements of the language like the other types,
\item
  \index{continuation}\emphDef{continuation}:
  abstract representation of the program state,
\item
  \index{first-class!first-class continuation}\emphDef{first-class continuation}:
  continuations are elements of the language like the other types,
\item
  \index{properly tail-recursive}\emphDef{properly tail-recursive}:
  tail-recursive calls are transformed to iterations,
\item delayed evaluation: allow lazy evaluation and call by need,
\item shared namespace for procedures and variables,
\item the order of evaluation of procedure arguments are not fixed,
\item primitive numbers are arbitrary large,
\item
  \index{macro!hygienic macro}\emphDef{hygienic macro}:
  there is no accidental capture of identifiers in macro expansion,
\item \dots
\end{itemize}


\subsection{\textit{Scheme} is a good target to experiment with static analyzers}
Only the essential core of the \index{RnRS@R$n$RS!R5RS}R5RS \cite{kelsey_revised5_1998}
will be considered here.
\footnote{
  Previous master thesis from ULB and VUB studied AAM
  for non deterministic Scheme extensions:
  concurrency by Quentin \textsc{Stiévenart} \cite{stievenart_static_2014}
  and \itblockquote{JavaScript-like objects and events}
  by Jonas \textsc{De Bleser} \cite{de_bleser_static_2016}.

  Note that Quentin \textsc{Stiévenart} had studied sequential AAM to analyze concurrent programs.
  The subject here is the opposite: to study parallelized AAM to analyze sequential programs.}
Next version of R$n$RS begins to complexify the language
and losts its minimalist design philosophy.
Nor do we consider macros.

To build a (concrete) \textit{Scheme} interpreter
only few fundamental forms have to be necessarily implemented.
The other forms of the language can be composed from these few fundamental forms.
Thus only few abstract forms have to be required to build an abstract interpreter.

\bigskip
Despite its simplicity \textit{Scheme} contains characteristics
\itblockquote{to support most of the major programming paradigms in use today}
\cite{kelsey_revised5_1998}.
We are particularly interested by its capacity to manipulate mutable data
(that causes side-effects),
closures and first-class functions,
because these possibilities of programming languages are difficult to statically analyze.

Its simplicity is a double advantage:
exploration on Scheme is simple to implemented,
and exploration on Scheme is representative for more complex programming languages.
For us it is a bit of a perfect language
to manipulate it.

\bigskip
More contextual, it is also a programming language taught at the VUB.

\bigskip
It is interesting to note that this simplicity had not been premeditated
by the creators of the language \cite{sussman_first_1998}.
They were planning something complicated, and realized that it was simple.
They discovered the powerful simplicity of the
\index{lambda-calculus@$\lambda$-calculus}
$\lambda$-calculus to express programming languages.
\textit{Scheme} may be summarized in this maxim:
\itblockquote{\textit{Scheme} is only syntactic sugar
  \footnote{
    \index{syntactic sugar}\emphDef{syntactic sugar}
    is unnecessary syntactic forms added for convenience.}
  on the $\lambda$-calculus.}
This mathematical model of computation
will be briefly presented in chapter \ref{chap:foundation}.



\section[The \textit{Scala} programming language, for the implementation of the AAM]
        {The \textit{Scala} programming language,\\
          for the implementation of the AAM}
\textit{Scala} is the programming language chosen by
Quentin \textsc{Stiévenart}
to implement \textit{Scala-AM} \cite{stievenart_scala-am:_2017} during his PhD.
We think that this is a good choice.

\bigskip
\index{Scala}\emphDef{Scala}
\cite{community_scala_????}
is also a functional programming language.
One of its great forces is to associate the two major paradigms usually opposed
that are the object paradigm and the functional paradigm.

Its standard implementation runs on the Java Virtual Machine (JVM),
so programs are portable.
Moreover it is possible to take advantage of all the rich \textit{Java} ecosystem.
By now, \textit{Java} also integrates functional concepts,
but \textit{Scala} is a more modern programming language,
directly designed to integrate this paradigm.
Its contains many useful features like static and strong typing with type inference,
immutability, pattern matching, first-class functions, lazy evaluation, etc.

\bigskip
\label{sec:scalachoice}%
The choice of \textit{Scala} is more important for the future work
as it is particularly well suited to parallel programming.
Aleksandar \textsc{Prokopec} explains
in its book \textit{Learning Concurrent Programming in Scala} \cite{prokopec_learning_2017}
that the nice features of \textit{Scala}
allows to implements various concurrency model in frameworks
as such manner that they can used like basic language features.
In the future work we will explore the actor model
with the \textit{Akka} framework \cite{community_akka_????}.

\bigskip
\textit{Scala} is less widespread than \textit{Java} but it is becoming more popular.
And like \textit{Scheme}, it is a programming language taught at the VUB.
\footnote{
  Martin \textsc{Odersky},
  the designer of the \textit{Scala} language
  proposes two interesting MOOCs to approach
  \textit{Scala} and the functional programming:
  \textit{Functional Programming Principles in Scala}
  \cite{odersky_functional_2016}
  and \textit{Functional Program Design in Scala}
  \cite{odersky_functional_2016-1}.}



\section{Abstracting Abstract Machines}
In a Web article \cite{might_what_2012} Matthew \textsc{Might}
shows how to implement an abstracting abstract machine (AAM),
step by step.
In this short article,
\textit{Scheme} (in fact \textit{Racket} \cite{community_racket_????})
is not used as the target language.
It is used to implement the AAM.
The language analyzed is the simple language of arithmetic expressions,
by the sign abstraction illustrated in the subsection \ref{subsec:sign} of this document.
\footnote{
  In a recording \cite{might_what_2014} of a lecture
  he shows that step by step, live.}

\bigskip
The method is quite straightforward.
It consists in implementing in practice
which was explained in theory during this chapter.

\bigskip
The first step is to define the semantics of arithmetic expressions.

Then he implements a tiny interpreter, an abstract machine (AM), for this concrete semantics.
\footnote{
  In the \textit{wizard book} \cite{abelson_structure_1996}
  a (concrete) \textit{Scheme} interpreter is implemented in \textit{Scheme} itself.}

\bigskip
After that he duplicates the interpreter
and he modifies it in order to adapt it to the abstract sign semantics.
The result is an AAM.
In fact it is just an AM for another semantics that is the abstract semantics.


\subsection{Scala-AM}
\textit{Scala-AM} \cite{stievenart_scala-am:_2017}
is a framework implemented in \textit{Scala} to build AAMs.
It is both a concrete interpreter and an abstract interpreter.
Its implementation was designed to be general and modular,
with the aim to support different semantics and abstractions.
An other aim was to facilitate the work on different aspects.
It is possible for
\itblockquote{language designers,
  static analysis developers
  and machine abstraction experts
  to combine their efforts.} \cite{stievenart_scala-am:_2016}

The paper \textit{Scala-AM: A Modular Static Analysis Framework}
\cite{stievenart_scala-am:_2016}
illustrates that with an AAM for a static analysis of Scheme.

\bigskip
As this implementation was not designed with the goal to be fast,
the future work will be to study this implementation
and will have to identify slow parts, to parallelize it.
Eventually better states representation will be explored
to facilitates the parallelization.
