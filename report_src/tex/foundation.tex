% -*- coding: utf-8 -*-
\chapter{Mathematical foundation}\label{chap:foundation}
This chapter exposes some mathematical structures
to have a well defined theoretical frame
of notions presented in the chapter \ref{chap:overview}.

\section[Partially ordered sets and lattices]{Partially ordered sets and lattices \footnotemark}
\footnotetext{
  This is the order notion of lattice
  (\foreignlanguage{french}{\textit{un treillis}} in French)
  and not the geometric notion
  (\foreignlanguage{french}{\textit{un réseau}} in French).}%
This somewhat indigestible section
summarizes basic definitions about partially ordered sets.
More details and properties are available
in the book of \textsc{Davey} and \textsc{Priestley} \cite{davey_introduction_2002}.

It is a part of the domain theory initiated by Dana \textsc{Scott} in the late 1960s
to formalized the denotational semantics of the $\lambda$-calculus.
These structures will be used to formalize abstractions
and the fixed point search.

\subsection{Partially ordered sets}
Let $S$ be a set.

\begin{definition}[Relation]
  \index{binary relation}%
  A \emphDef{binary relation} on $S$
  is a subset of $S \times S$.
\end{definition}

\begin{definition}[Partially order]
  \index{order!partially order}%
  \index{poset}%
  \index{ partially order@$\porder$}%
  An \emphDef{partially order} (or \emphDef{poset}) on $S$
  is a binary relation \emphDef{$\porder$} on $S$
  such that, $\forall x, y, z \in S$:
  \begin{enumerate}
  \item
    $x \porder x$
    \hspace{11.6em}\index{reflexivity}(\emphDef{reflexivity})
  \item
    $(x \porder y) \conj (y \porder x) \Impl (x = y)$
    \qquad\index{antisymmetry}(\emphDef{antisymmetry})
  \item
    $(x \porder y) \conj (y \porder z) \Impl (x \porder z)$
    \hspace{2.3em}\index{transitivity}(\emphDef{transitivity})
  \end{enumerate}

  \medskip
  \index{set!ordered set!partially ordered set}%
  \index{ (S,order)@$(S, \porder)$}%
  An \emphDef{partially ordered set}
  \emphDef{$(S, \porder)$}
  is a set $S$ with an order $\porder$.
\end{definition}

The idea is that $x$ and $y$ are not necessarily comparable,
but if they are then $x \porder y$ expresses that $x$ is lower or equal to $y$.

The set $S$ may be any set.
However we will use these concepts with sets of sets
(in particular with powersets).
That is why we use the symbol $\porder$
(and others symbols in the following)
that looks like the inclusion symbol $\subseteq$.

\medskip
$\forall$ set $E : (\powerset(E), \subseteq)$ is a partially ordered set.

\bigskip
\index{Hasse diagram@\textsc{Hasse} diagram}%
A \emphDef{\textsc{Hasse} diagram}
is a simple way to represent a finite partially order set.
Each element is represented by a vertex
and each edge between two vertices $x$ and $y$
represents $x \porder y$ if the edge  goes upward from $x$ to $y$.
Self-loop from the reflexivity property
and all $x \porder y$ such that can be deduced from transitivity are not represented.

\begin{figure}[H]
  \centering
  \input{tikz/hasse_diagram_powerset.tex}\hfil
  \input{tikz/hasse_diagram_set.tex}
  \caption[\textsc{Hasse} diagrams of the partially ordered set\dots]
          {\textsc{Hasse} diagrams of the partially ordered set $(\powerset(\{a, b, c\}), \subseteq)$\\
            and of the partially ordered set $(\{\emptyset,
            \{a\}, \{c\},
            \{a, c\}, \{b, c\},
            \{a, b, c\}\}, \porder)$
            with $\porder = \{(\emptyset, \{a\}),
            (\emptyset, \{a, c\}),
            (\emptyset, \{a, b, c\}),
            (\{a\}, \{a, c\}),
            (\{a\}, \{a, b, c\}),
            (\{b, c\}, \{a, b, c\})\}$.}
\end{figure}

\bigskip
Let $(S, \porder)$ be a partially ordered set.

\begin{definition}[Totally ordered set]
  \index{set!ordered set!totally ordered set}%
  \index{chain}%
  $(S, \porder)$ is a \emphDef{totally ordered set} (or a \emphDef{chain})
  if $\forall x, y \in S : x$ and $y$ are
  \index{comparable}\emphDef{comparable} (i.e. $x \porder y$ or $y \porder x$).
\end{definition}

\bigskip
In general the order structures what we manipulate are not total.
We have following properties to help us to deal with them.

\begin{definition}[Upper bound]\mbox{}\\
  \index{bound!upper bound}%
  $\forall X \subseteq S, u \in S$ is an
  \emphDef{upper bound of $X$}
  if $\forall x \in X, x \porder u$.

  \medskip
  \index{bound!upper bound!least upper bound}%
  \index{supremum}%
  \index{ join X@$\join X$}%
  \index{join}%
  \index{ x join y@$x \join y$}%
  $\forall X \subseteq S, u \in S$ is the \footnotemark{}
  \emphDef{least upper bound} (or \emphDef{supremum}) \emphDef{of $X$}
  if $u$ is an upper bound of $X$
  and $\forall$ upper bound $u'$ of $X : u \porder u'$.\\
  We will noted it by \emphDef{$\join X$},
  and we will called $\join$ the \emphDef{join} operator.\\
  We will abbreviate $\join\{x, y\} =$ \emphDef{$x \join y$}.

  \medskip
  \index{top}%
  \index{_top@$\top$}%
  If there exists,
  we called \emphDef{top}
  the element $\emphDef{\top} = \join L$.
\end{definition}
\footnotetext{
  The unicity is a direct consequence of definitions and the antisymmetry property.}

Dually we define,
\begin{definition}[Lower bound]\mbox{}\\
  \index{bound!lower bound}%
  $\forall X \subseteq S, l \in S$ is a
  \emphDef{lower bound of $X$}
  if $\forall x \in X, l \porder x$.

  \medskip
  \index{bound!lower bound!greatest lower bound}%
  \index{infimum}%
  \index{ meet X@$\meet X$}%
  \index{meet}%
  \index{ x meet y@$x \meet y$}%
  $\forall X \subseteq S, l \in S$ is the
  \emphDef{greatest lower bound} (or \emphDef{infimum}) \emphDef{of $X$}
  if $l$ is an lower bound of $X$
  and $\forall$ lower bound $l'$ of $X : l' \porder l$.\\
  We will noted it by \emphDef{$\meet X$},
  and we will called $\meet$ the \emphDef{meet} operator.\\
  We will abbreviate $\meet\{x, y\} =$ \emphDef{$x \meet y$}.

  \medskip
  \index{bottom}%
  \index{_bottom@$\bottom$}%
  If there exists,
  we called \emphDef{bottom}
  the element $\emphDef{\bottom} = \meet L$.
\end{definition}

\begin{definition}[Directed set]
  \index{set!directed set}%
  $(S, \porder)$ is a \emphDef{directed set}
  if $\forall x, y \in S : x \join y$ exists.
\end{definition}

\begin{definition}[CPO]
  \index{CPO}%
  \index{set!ordered set!partially ordered set!complete partially ordered set}%
  $(S, \porder)$ is a \emphDef{complete partially ordered set} (\emphDef{CPO})
  if
  \begin{enumerate}
  \item
    the bottom $\bottom$ element exists,
  \item
    $\forall X \subseteq S : \join X$ exists.
  \end{enumerate}
\end{definition}


\subsection{Lattices}
Let $(L, \porder)$ be a non-empty partially ordered set.

\begin{definition}[Lattice]
  \index{lattice}%
  $(L, \porder)$ is a \emphDef{lattice}
  if $\forall x, y \in L : x \join y$ and $x \meet y$ exist.
\end{definition}

\begin{definition}[Complete lattice]\mbox{}\\
  \index{lattice!complete lattice}%
  $(L, \porder)$ is a \emphDef{complete lattice}
  if $\forall X \subseteq L : \join X$ and $\meet X$ exist.
\end{definition}

If $L$ is a finite set,
then every lattice $(L, \subseteq)$
is a complete lattice.

\medskip
$\forall$ set $E : (\powerset(E), \subseteq)$ is a complete lattice
where $\forall X \subseteq \powerset(E) : \join X = \cup X$ and $\meet X = \cap X$.

\bigskip
Note that the lattice structure can be also seen as an algebraic structure, equivalent.
Let $(L, \porder)$ be a lattice.

\begin{lemma}[Connecting lemma]
  $\forall x, y \in L :$\\
  $(x \porder y) \iff (x \join y = x) \iff (x \meet y = y)$
\end{lemma}

\begin{theorem}[Algebraic properties of join and meet]
  $\forall x, y, z \in L :$\\[-3ex]
  \begin{enumerate}
  \item
    $(x \join y) \join z = x \join (y \join z)$
    \qquad\index{law!associative law}(\emphDef{associative laws})\\
    $(x \meet y) \meet z = x \meet (y \meet z)$
  \item
    $x \join y = y \join x$
    \hspace{6.4em}\index{law!commutative law}(\emphDef{commutative laws})\\
    $x \meet y = y \meet x$
  \item
    $x \join (x \meet y) = x$
    \hspace{5.8em}\index{law!absorption law}(\emphDef{absorption laws})\\
    $x \meet (x \join y) = x$
  \item
    $x \join x = x$
    \hspace{7.95em}\index{law!idempotency law}(\emphDef{idempotency laws})\\
    $x \meet x = x$
  \end{enumerate}
\end{theorem}

\begin{theorem}[Lattice equivalence algebraic point of view]
  Let $(L, \join, \meet)$ be a non-empty set with two binary relations
  that satisfy all properties of the previous theorem.\\
  Let a binary relation $\porder$ such that
  $\forall x, y \in L : (x \porder y) \iff (x \join y) = y$.\\
  Then $(L, \porder)$ is a lattice,
  such that $\join$ and $\meet$ are the two operators defined like before.
\end{theorem}


\subsection{Fixed points}
Let $(S, \porder)$ be a partially ordered set,
let $f : S \function S$ be a function.

\begin{definition}[Fixed point]\mbox{}\\
  \index{fixed point}%
  $x \in S$ is a \emphDef{fixed point} of $f$ if $f(x) = x$.

  \medskip
  \index{fixed point!least fixed point}%
  $x \in S$ is a \emphDef{least fixed point} of $f$\\
  if $x$ is a fixed point
  and $\forall y \in S : (y$ is a fixed point$) \Impl (x \porder y)$.

  \index{ lfp(f)@$\lfp(f)$}%
  If there exists, we note it \emphDef{$\lfp(f)$}.

  \medskip
  \index{fixed point!greatest fixed point}%
  $x \in S$ is a \emphDef{greatest fixed point} of $f$\\
  if $x$ is a fixed point
  and $\forall y \in S : (y$ is a fixed point$) \Impl (y \porder x)$.

  \index{ gfp(f)@$\gfp(f)$}%
  If there exists, we note it \emphDef{$\gfp(f)$}.
\end{definition}

Let $(S, \porder_S)$ and $(T, \porder_T)$ be two partially ordered sets.
\begin{definition}[Order-preserving function]
  \index{function!order-preserving function}%
  \index{function!monotone function}%
  The function $f : S \function T$
  is said \emphDef{order-preserving} (or \emphDef{monotone}) if
  $\forall x, y \in S : (x \porder_S y) \Impl (f(x) \porder_T f(y))$.
\end{definition}


Let $(S, \porder_S)$ and $(T, \porder_T)$ be two CPOs.
\begin{definition}[Continuous function]\mbox{}\\
  \index{function!continuous function}%
  The function $f : S \function T$
  is said \emphDef{continuous} \footnotemark{}
  if
  $\forall$ directed set $X \subseteq S :$
  \begin{enumerate}
  \item
    $f(X)$ is directed,
  \item
    $f(\join X) = \join f(X)$.
  \end{enumerate}
\end{definition}
\footnotetext{
  The analysis notion of continuity preserves limits.
  The notion here of \textsc{Scott} continuity
  preserves the join of a directed set.}
This \itblockquote{continuity condition can be awkward to check.}
\cite{davey_introduction_2002}

\enlargethispage{\baselineskip}%
\begin{theorem}[\textsc{Knaster}–\textsc{Tarski} fixed point]
  Let $(L, \porder)$ be a complete lattice
  and $f : L \function L$ an order-preserving function.

  Then $\gfp(f) = \join \{x \in L \mid x \porder f(x)\}$
  and $\lfp(f) = \meet \{x \in L \mid f(x) \porder x\}$.
\end{theorem}

\begin{theorem}[\textsc{Kleene} fixed point] \cite{bouissou_analyse_2008}\\
  Let $(L, \porder)$ be a complete lattice with a bottom $\bottom$,
  and $f : L \function L$ a continuous function.

  Then $\lfp(f) = \join\limits_{i\in\naturals} \{f^i(\bottom)\}$.

  \medskip
  With a top $\top$.

  Then $\gfp(f) = \meet\limits_{i\in\naturals} \{f^i(\top)\}$.
\end{theorem}



\section{Back to the notion of abstraction}
\epigraph{``\textit{Abstract Interpretation,
    one of the most applied techniques for semantics based static analysis of software,
    is based on two main key-concepts:
    the correspondence between concrete
    and abstract semantics through \textsc{Galois} connections/insertions,
    and the feasibility of a fixed point computation of the abstract semantics,
    through the fast convergence of widening operators.
    The latter point is crucial to ensure the scalability of the analysis
    to large software systems.}''}
         {--- Agostino \textsc{Cortesi},\\
           \textit{Widening Operators for Abstract Interpretation} \cite{cortesi_widening_2008}}
\noindent
Let us leave the difficult problem of the widening operator, eventually for future work.


\begin{definition}[\textsc{Galois} connection]
  \index{Galois connection@\textsc{Galois} connection}%
  A \emphDef{\textsc{Galois} connection}\footnotemark{}
  between two partially ordered sets $(A, \porder_A)$ and $(B, \porder_B)$
  is a pair of functions $\abstraction : A \function B$ and $\concretization : A \function B$
  such that
  $\forall a \in A, b \in B : \abstraction(a) \porder_B b \iff a \porder_A \concretization(b)$.
\end{definition}
\footnotetext{
  An other possible definition sometimes used in some contexts reverses the order.
  Here the definition used preserve the order.}

\bigskip
The concept of the \textsc{Galois} connection
give us a general framework to formalized
the notion of abstraction seen in the chapter \ref{chap:overview}.
Let $X$ be a concrete set, i.e. a set of values that we want analysis.
$(\powerset(X), \subseteq)$ is a complete lattice.

Note $\abst{X}$ the abstraction set of $X$.
Suppose that $\abst{X}$ is such that $(\abst{X}, \porder)$ is a partially ordered set.

Let $\abstraction$ and $\concretization$ be a \textsc{Galois} connection between
$(\powerset(X), \subseteq)$ and $(\abst{X}, \porder)$.

\begin{tikzpicture}[->,>=stealth',node distance=5em]
  \node (1) {$\powerset(X)$};
  \node (2) [right of=1] {$\abst{X}$};

  \path ([yshift=2pt]1.east) edge node[above] {$\abstraction$} ([yshift=2pt]2.west);
  \path ([yshift=-2pt]2.west) edge node[below] {$\concretization$} ([yshift=-2pt]1.east);

\end{tikzpicture}

We call $\abstraction$ the abstraction function
and $\concretization$ the concretization function.

Next the operations on $X$ must be also abstracted.


\paragraph{Sign example}
Formalize the sign example seen in the subsection \ref{subsec:sign}
with this framework.

$\abst{sign} = \{\bottom, +, \abst{0}, -, \top\}$
abstracts the set of integers $\integers$
and form a complete lattice
with the partially order characterized by the following \textsc{Hasse} diagram.
\begin{figure}[H]
  \centering
  \input{tikz/hasse_diagram_sign.tex}
  \caption{\textsc{Hasse} diagram of the complete lattice of signs.}
\end{figure}

The total abstraction and concretization functions:\\[1ex]
$\begin{array}[t]{@{}l@{\ }lcl@{}}
  \abstraction : & \powerset(\integers) & \function & \abst{sign}\\
  & x & \mapsto & \left\{\begin{array}{ll@{}}
    \bottom & \text{if }x = \emptyset\\
     - & \text{if }x \subseteq -\naturals*\\
     \abst{0} & \text{if }x = \{0\}\\
     + & \text{if }x \subseteq \naturals*\\
     \top & \text{otherwise}\\
  \end{array}\right.
\end{array}$\hfill
$\begin{array}[t]{l@{\ }lcl@{}}
  \concretization : & \abst{sign} & \function & \powerset(\integers)\\
  & y & \mapsto & \left\{\begin{array}{ll@{}}
    \emptyset & \text{if }y = \bottom\\
     -\naturals* & \text{if }y = -\\
     \{0\} & \text{if }y = \abst{0}\\
     \naturals* & \text{if }y = +\\
     \integers & \text{if }y = \top\\
  \end{array}\right.
\end{array}$

\medskip
The abstraction of the addition operation (presented only for some values):\\[1ex]
$\begin{array}{@{}l@{\ }lcl@{}}
  \abst{+} : & \abst{sign} \times \abst{sign} & \function & \abst{sign}\\
  & (a, b) & \mapsto & \left\{\begin{array}{ll@{}}
    - & \text{if }a = b = -\\
    \top & \text{if }a = -\text{ and }b = +\\
     & \hspace{-2em}\text{(The corresponding example to }\{-, 0\}\,\abst{+}\,\{-\} = \{-\}\\
     & \hfill\text{is not representable with this sign abstraction.)}\\
    \dots\\
  \end{array}\right.
\end{array}$

\medskip
If we analysis again the sign of the $-42 + 42$,\\
we have $\alpha(\{-42\}) = -$ and $\alpha(\{42\}) = +$.\\
Thus $\alpha(\{-42\})\ \abst{+}\ \alpha(\{42\}) = \top$\\
and if we come back to the concrete side:
$\gamma\left(\alpha(\{-42\})\ \abst{+}\ \alpha(\{42\})\right) = \integers$.

We see that the abstraction version of the addition operation
is clearly less precise that the direct concrete operation: $-42 + 42 = 0$.

Nevertheless the result is an over-approximation:
$\{0\} \subset (\concretization \circ \abst{+} \circ \abstraction) = \integers$

\paragraph{Sound abstraction}
Let $f : X \function X$ be an operation in $X$.
We say that the abstraction is
\index{sound}\emphDef{sound} for this operation
if
$\forall x \in X: \alpha(f(x)) \porder \abst{f}(\alpha(x))$,
i.e. if the abstract operation over-approximates the concrete operation.

\begin{figure}[H]
  \centering
  \input{tikz/sound_abstraction.tex}
  \caption{Sound abstraction.}
\end{figure}


\paragraph{Future work}
We guess that the interest of the \textsc{Galois} connection
is the good properties of the combinations of $\abstraction$
and $\concretization$ functions
which allow the application of fixed point theorems.
Maybe these slides \cite{sutre_summer_2008}
of the presentation
\textit{Summer School on Verification Technology, Systems \& Applications --- part 2}
by Grégoire \textsc{Sutre}
will be a good first approach to learn more about that.



\section[Lambda-calculus]{$\lambda$-calculus}
\label{sec:lambdacalculus}%
Only few words on the
\index{lambda-calculus@$\lambda$-calculus}\emphDef{untyped $\lambda$-calculus}.
It is a very simple mathematical model of computation,
however equivalent in terms of calculability to the \textsc{Turing} machines.
\index{Turing machine@\textsc{Turing} machine}%
Consequently equivalent to all our computers and programming languages.
\footnote{
  In fact our computers are limited by time and memory,
  in contrary to a \textsc{Turing} machine,
  that is a mathematical ``object''.
  However ideal versions of computers are completely equivalent to a \textsc{Turing} machine.\\
  Programming languages are also mathematical ``objects'',
  and theirs implementations have the same limitations that computers.}
\footnote{
  As all known calculability models are equivalent each others
  (with the exception of models that are too simple),
  the \textsc{Church}–\textsc{Turing} thesis proclaims that
  this notion of calculability corresponds exactly to the intuitive notion of calculability.}

\medskip
Simple description of the grammar, in the usual \textsc{Backus}–\textsc{Naur} form:
\begin{definition}[Untyped $\lambda$-calculus]\mbox{}\\[-5ex]
  \index{lambda-calculus@$\lambda$-calculus}%
  \begin{bnf*}
    \bnfprod{v}{\bnftd{\normalfont variable identifier} \in Var}\\
    \bnfprod{e}
            {\bnfpn{v}
             \bnfor (\lambda \bnfpn{v}.\bnfpn{e})
             \bnfor (\bnfpn{e}\ \bnfpn{e})}
  \end{bnf*}
\end{definition}

\begin{tabular}{@{}lll}
  $v$ & variable\\
  $(\lambda v.e)$ & abstraction & Definition of a function.\\
  $(e\ e')$ & application & Applying the function $e'$ to the argument $e$.\\
\end{tabular}

\bigskip
The syntax of \textit{Scheme} is only composed
by \index{S-expression}\emphDef{S-expressions},
i.e. by parenthesized lists
such that the first element is the operator
and the following are its arguments.
This corresponds directly to the $\lambda$-calculus.
\begin{definition}[S-expression]\mbox{}\\[-5ex]
  \begin{bnf*}
    \bnfprod{atom}{\bnftd{\normalfont indivisible fundamental item}}\\
    \bnfprod{S-expression}
            {(\bnfpn{S-expression}\ .\ \bnfpn{S-expression})}
  \end{bnf*}
\end{definition}
